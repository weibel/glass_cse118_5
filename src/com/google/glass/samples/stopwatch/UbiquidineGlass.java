/*
 * Copyright (C) 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.glass.samples.stopwatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;

/**
 * StopWatch sample's main activity.
 */
public class UbiquidineGlass extends Activity {

	private String display = "testing";
	private Thread mBackgroundUpdater;
	private UpdateTableStatus mAsyncCheck;
	private UpdateTableStatus mAsyncReset;
	private static final String mChecktable = "http://ubiquidine.azurewebsites.net/HttpHandler/checktable";
	private static final String mResettable = "http://ubiquidine.azurewebsites.net/HttpHandler/resettable";
	private static final String mAllTablesGood = "All tables look good!";
	URL mUrl;
	private TextView mText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			mUrl = new URL(mChecktable);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		mBackgroundUpdater = new Thread(new Runnable() {
			public void run() {
				while (true) {
					updateTableStatus();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
							e.printStackTrace();
					}
				}
			}
		});
		
		UbiquidineGlass.this.runOnUiThread(new Runnable() {
			public void run() {
				setContentView(R.layout.layout_stopwatch);
				mAsyncCheck = new UpdateTableStatus();
				mAsyncReset = new UpdateTableStatus();
				mText = (TextView) findViewById(R.id.textView1);
				mText.setText(display);
				mText.setText(mAllTablesGood);
				mBackgroundUpdater.start();
			}
		});

	}

	/**
	 * Handle the tap event from the touchpad.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// Handle tap events.
		case KeyEvent.KEYCODE_DPAD_CENTER:
		case KeyEvent.KEYCODE_ENTER:
			resetTable();			
			return true;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		updateTableStatus();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public static String convertStreamToString(InputStream inputStream)
			throws IOException {
		if (inputStream != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(
						inputStream, "UTF-8"), 1024);
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				inputStream.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}
	
	private void resetTable() {
		if(this.mAsyncReset.getStatus() != AsyncTask.Status.RUNNING) {
			mAsyncReset = new UpdateTableStatus();
			this.mAsyncReset.execute(mResettable);
		}
	}
	
	private void updateTableStatus() {
		if(this.mAsyncCheck.getStatus() != AsyncTask.Status.RUNNING) {
			mAsyncCheck = new UpdateTableStatus();
			this.mAsyncCheck.execute(mChecktable);
		}
	}
	

	private class UpdateTableStatus extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... url) {
			for (String u : url) {
				HttpResponse response = null;
				try {
					HttpClient client = new DefaultHttpClient();
					HttpPost request = new HttpPost();
					request.setURI(new URI(u));
					response = client.execute(request);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					InputStream content = response.getEntity().getContent();
					if(u.equals(mResettable)) {
						return evaluateResult(null);
					}
					return evaluateResult(convertStreamToString(content));
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		
		protected String evaluateResult(String tableStatus)
		{
			if(tableStatus == null || tableStatus.equals("000")) {
				return mAllTablesGood;
			}
			StringBuilder result = new StringBuilder("Table 1 needs:\n");
			if (tableStatus.charAt(0) == '1')
			{
				result.append("- a refill.\n");
			}
			if (tableStatus.charAt(1) == '1')
			{
				result.append("- a waiter.\n");
			}
			if (tableStatus.charAt(2) == '1')
			{
				result.append("- a plate cleared.");
			}
			
			return result.toString(); 
		}
		
		
		@Override
		  protected void onPostExecute(String result) {
			if(result != null) {
				mText.setText(result);
			}
		}	

	}

}
